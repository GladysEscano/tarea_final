/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

/**
 *
 * @author Gladys Escaño
 */
public class Deportista extends Persona {
    
    public Deportista(){
        this.setNombre("Gladys Stefany Gutierrez Escaño");
        this.setIdentidad("06199-1998-00157");        
        this.setCelular("+504 3244-5213");
        this.setProfesion("Futbolista");
        this.setEdad(23);
        this.setRendimiento("ALTO");
    }
    public void imprimir()
    {
        System.out.println(
                "Nombre: "+this.getNombre()+
                "\nIdentidad: "+this.getIdentidad()+
                "\nCelular: "+this.getCelular()+
                "\nEdad: "+this.getEdad()+
                "\nProfesion: "+this.getProfesion()+
                "\nRendimiento: "+this.getRendimiento());
    }
}

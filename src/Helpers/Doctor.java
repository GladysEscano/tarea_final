/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

/**
 *
 * @author Gladys Escaño
 */
public class Doctor extends Persona {
    
    public Doctor (){
        this.setNombre("Gladys Stefany Gutierrez Escaño");
        this.setIdentidad("0015-1995-02482");
        this.setCelular("+504 8974-5624");
        this.setProfesion("MEDICINA");
        this.setEdad(22);
        this.setRendimiento("BAJO");
    }
    public void imprimir()
    {
        System.out.println("Nombre: "+this.getNombre()+"\nIdentidad:"+this.getIdentidad()+"\nCelular: "+this.getCelular()+"\nEdad: "+this.getEdad()+"\nProfesion: "+this.getProfesion()+"\nRendimiento: "+this.getRendimiento());
        System.out.println("");
    }
}
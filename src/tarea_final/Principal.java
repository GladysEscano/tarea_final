/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea_final;

import Helpers.Deportista;
import Helpers.Doctor;
import Helpers.Persona;

/**
 *
 * @author Gladys Escaño
 */
public class Principal {
    public Principal(){
      Persona p[]=new Persona[2];
      p[0]=new Doctor();
      p[1]=new Deportista();
      for(int x=0;x<p.length;x++)
          p[x].imprimir();
    }
    public static void main(String args[]){
        new Principal();
    }
}
